import React, { useEffect } from 'react';
import { NextPage } from 'next'
import { observer, inject } from 'mobx-react';
import { apartamentsStoreType } from '../store/Apartments';
import MainPage from './MainPage';

type Props = {
  apartments?: apartamentsStoreType,
}

// TODO replace by MainPage ?
const App: NextPage<Props> = inject('apartments')
  (observer((props: Props) => {
    return (
      <div>
        Test
        <MainPage />
      </div>
    );
  }));

export default App;
