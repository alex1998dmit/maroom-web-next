import App, { Container } from 'next/app';
import React from 'react';
import withMobxStore from '../lib/with.mobx.store';
import { Provider } from 'mobx-react';
import { storesType } from '../store';
import Layout from '../components/Layout';

interface IProps { 
  mobxStore: storesType
};

class MyApp extends App<IProps> {
  render() {
    const {Component, pageProps, mobxStore} = this.props;
    return (
      <Container>
        <Provider {...mobxStore}>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </Provider>
      </Container>
    );
  };
};

export default withMobxStore(MyApp);
