import React, { useEffect} from "react";
import { NextPage } from 'next'
import { observer, inject } from 'mobx-react';
import { apartamentsStoreType } from '../../store/Apartments';
import Page from "../../components/common/Page";
import FirstScreen from "./FirstScreen/FirstScreen";
import CurrentOffersScreen from "./CurrentOffersScreen/CurrentOffersScreen";
// import SoonAvailableScreen from "./SoonAvailableScreen/SoonAvailableScreen";
// import InfoBlock from "./InfoBlock/InfoBlock";
// import RoomsScreen from "./RoomsScreen/RoomsScreen";
// import EmailOrderScreen from "./EmailOrderScreen/EmailOrderScreen";
import JSONPretty from 'react-json-pretty';

type Props = {
  apartments?: apartamentsStoreType,
}

const MainPage: NextPage<Props> = inject('apartments')
  (observer((props: Props) => {
  const { fetchApartments, apartments, state } = props.apartments;
  useEffect(() => {
    const asyncFunctions = async () => {
      await fetchApartments();
    }
    asyncFunctions();
  }, []);
  return (
    <>
      <Page
        alwaysWhiteHeader={false}
        hideHeaderOnScroll={false}
      >
        <FirstScreen apartments={apartments}/>
        {/* <CurrentOffersScreen apartments={apartments}/> */}
        {/* <JSONPretty json={apartments} /> */}
        {/* <SoonAvailableScreen apartments={apartments}/> */}
        {/* <InfoBlock/> */}
        {/* <RoomsScreen apartments={apartments}/> */}
        {/* <EmailOrderScreen/> */}
      </Page>
    </>
  )
}));

export default MainPage;
