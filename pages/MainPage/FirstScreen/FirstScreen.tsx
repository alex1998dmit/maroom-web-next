import React, { Fragment, useRef, useState} from "react";
import { NextPage } from 'next'
import { observer, inject } from 'mobx-react';
import CircleButton, {CircleButtonTheme} from "../../../components/common/ui/CircleButton/CircleButton";
import CloseIcon from "../../../assets/close.svg";
import OpenIcon from "../../../assets/add.svg";
import Text, {TextSize, TextWeight} from "../../../components/common/ui/Text/Text";
import useWindowWidth from "../../../hooks/useWindowWidth";
import Container from "../../../components/common/ui/Container/Container";
import Button, {ButtonTheme, ButtonSize} from "../../../components/common/ui/Button/Button";
import FirstScreenApartmentCard from "./ApartmentCard/FirstScreenApartmentCard";
import classNames from "classnames";
import declOfNum from '../../../utils/declOfNum';
import s from './FirstScreen.module.scss';
import { citiesStoreType} from '../../../store/Cities';
import { apartamentsStoreType } from '../../../store/Apartments';


type Props = {
  cities?: citiesStoreType,
  apartments: apartamentsStoreType,
}

const FirstScreen: NextPage<Props> = inject('cities')
  (observer((props: Props)=> {
  const { data } = props.cities;
  const imageRef = useRef<null | HTMLDivElement>(null);
  const toggleButtonElRef = useRef<null | HTMLButtonElement>(null);

  // const scroll = useScroll();
  const width = useWindowWidth();

  const [isCardOpened, setIsCardOpened] = useState(false);

  const apartment = props.apartments.count ? props.apartments.data[0] : null;
  const rightElHeight = imageRef.current ? imageRef.current.offsetHeight : 600;
  const imgSrc = apartment && apartment.primary_pictures[0].uri;
  const isDesktop = width > 1024;
  const deviceClass = isDesktop ? s.desktop : s.mobile;
  const visibleCities = data.slice(0, width >= 1200 ? 3 : 2);
  // const isFixed = scroll && scroll.pos && scroll.pos.y < rightElHeight - window.innerHeight;

  const setToggleButtonRef = (ref: HTMLButtonElement) => {
    toggleButtonElRef.current = ref;
  };

  const toggleCard = () => {
    setIsCardOpened(!isCardOpened);
  };

  return (
    <div className={classNames(s.firstScreen, deviceClass)}>
      <Container className={s.actionsWrapper}>
        <div className={s.actionsContainer}>
          <Text weight={TextWeight.bold} size={isDesktop ? TextSize.h1 : TextSize.h2}>Сдаем, контролируем и{'\u00A0'}обслуживаем квартиры</Text>
          <div className={s.actions}>
            <Button
              className={s.action}
              size={ButtonSize.l}
              theme={ButtonTheme.primary}
            >Сдать{isDesktop ? ' квартиру' : null}</Button>
            <Button
              className={s.action}
              theme={ButtonTheme.outline}
              size={ButtonSize.l}
            >Снять{isDesktop ? ' квартиру' : null}</Button>
          </div>
        </div>
      </Container>
      <div className={s.citiesWrapper}>
        <Container className={s.citiesContainer}>
          <div className={classNames(s.citiesContent, isDesktop && s.desktop)}>
            <Text className={s.citiesTitle} size={TextSize.h2} weight={TextWeight.bold}>Мы работаем в</Text>
            <div className={s.cities}>
              {visibleCities.map((city) => {
                const renderCount = () => {
                  if (!city.isAvailable) return 'Скоро';
                  const count = city.apartments_count;
                  const label = declOfNum(count, ['квартира', 'квартиры', 'квартир']);
                  return `${count} ${label}`
                };

                return (
                  <div className={classNames(s.city, isDesktop && s.desktop)} key={city.name}>
                    <div className={s.cityImage} style={{ backgroundImage: `url(${city.image})` }}/>
                    <Text className={s.cityName} size={TextSize.h3}>{city.name}</Text>
                    <Text className={s.cityCount} size={TextSize.s}>
                      {renderCount()}
                    </Text>
                  </div>
                )
              })}
            </div>
          </div>
        </Container>
      </div>
      {/* {isDesktop && apartment ? (
        <div className={s.assetImage} style={{ backgroundImage: `url(${imgSrc})` }} ref={imageRef}>
          <div className={classNames(s.cardContainer, isFixed ? s.fixed : null)}>
            { isCardOpened ? (
              <FirstScreenApartmentCard apartment={apartment}/>
            ) : null }
            <CircleButton
              innerRef={setToggleButtonRef}
              className={s.toggleCardButton}
              theme={isCardOpened ? CircleButtonTheme.outline : CircleButtonTheme.primary}
              icon={isCardOpened ? CloseIcon : OpenIcon}
              onClick={toggleCard}
            />
          </div>
        </div>
      ) : null} */}
    </div>
  )
}));

export default FirstScreen;
