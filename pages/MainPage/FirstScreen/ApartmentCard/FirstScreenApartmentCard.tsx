import s from './ApartmentCard.module.scss';
import React, {FC, Fragment} from "react";
import {ApartmentMock, ApiApartment} from "../../../../../types/Apartments";
import ApartmentCard from "../../../../common/ApartmentCard/ApartmentCard";
import Button, {ButtonIconPosition, ButtonSize, ButtonTheme} from "../../../../common/ui/Button/Button";
import {ReactComponent as LongArrow} from "../../../../../assets/longArrow.svg";

type Props = {
  apartment: ApiApartment,
};

const FirstScreenApartmentCard: FC<Props> = (props) => {
  const renderButton = () => {
    return (
      <Button
        className={s.apartmentCardButton}
        icon={LongArrow}
        iconPosition={ButtonIconPosition.right}
        theme={ButtonTheme.primary}
        size={ButtonSize.s}
      >Посмотреть</Button>
    );
  };

  return (
    <Fragment>
      <ApartmentCard
        className={s.apartmentCard}
        apartment={props.apartment}
        additionalInfo={renderButton()}
      />
      <div className={s.arrow}/>
    </Fragment>
  );
};

  export default FirstScreenApartmentCard;
