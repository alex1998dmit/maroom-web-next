import React, {FC, Fragment, useEffect} from "react";
import Container from "../../../components/common/ui/Container/Container";
import s from './CurrentOffersScreen.module.scss';
// import ApartmentsSlider from "../../../components/common/AppartmentsSlider/ApartmentsSlider";
import useWindowWidth from "../../../hooks/useWindowWidth";
// import ApartmentCardWithGallery from "../../../components/common/ApartmentCard/ApartmentCardWithGallery/ApartmentCardWithGallery";
import Text, {TextSize, TextWeight} from "../../../components/common/ui/Text/Text";
import { apartamentsStoreType } from '../../../store/Apartments';
// import {ApiApartment} from "../../../../types/Apartments";

type Props = {
  apartments: apartamentsStoreType
}

const CurrentOffersScreen: FC<Props> = (props) => {
  const width = useWindowWidth();
  const isDesktop = width > 991;

  console.log(props.apartments);
  const firstApartments = props.apartments.data.slice(0, 3);

  return (
    <div className={s.currentOffersScreen}>
      <Container>

        {isDesktop ? (
          dsad
          // <ApartmentsSlider
          //   title='Актуальные предложения'
          //   className={s.slider}
          //   apartments={props.apartments}
          //   showAllApartmentsButton={true}
          //   total={props.apartments.length}
          //   allApartmentsLink='#'
          // />
        ) : (
          <Fragment>
            <Text size={TextSize.h2} weight={TextWeight.bold}>Актуальные предложения</Text>
            {/* {firstApartments.map((apartment: object, index: number) => {
              return (
                <>ds</>
                // <ApartmentCardWithGallery
                //   key={index}
                //   apiRef={(api) => api.init()}
                //   className={s.card}
                //   apartment={apartment}
                // />
              )
            })} */}
          </Fragment>
        )}
      </Container>
    </div>
  );
};

export default CurrentOffersScreen;
