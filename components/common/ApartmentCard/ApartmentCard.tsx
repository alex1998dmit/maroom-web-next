import React, {FC, ReactNode} from "react";
import {ApiApartment} from '../../../interfaces/apartments';
import Card, {CardSection} from "../ui/Card/Card";
import s from './ApartmentCard.module.scss';
import Text, {TextColor, TextSize, TextWeight} from "../ui/Text/Text";
import classNames from "classnames";
import Icon from "../Icon/Icon";
import {ReactComponent as DoorIcon} from "../../../assets/door.svg";
import {ReactComponent as SquareIcon} from "../../../assets/square.svg";
import {ReactComponent as StairsIcon} from "../../../assets/stairs.svg";

type Props = {
  apartment: ApiApartment,
  withBorder?: boolean,
  additionalInfo?: ReactNode
  before?: ReactNode,
  after?: ReactNode,
  className?: string,
};

const ApartmentCard: FC<Props> = (props) => {
  const {
    apartment,
    before = null,
    after = null
  } = props;

  return (
    <Card
      className={classNames('apartmentCard', props.className, s.apartmentCard)}
      withBorder={props.withBorder}
    >
      {before}
      <CardSection>
        <div className={s.addressWrapper}>
          <div className={s.addressContainer}>
            <Text size={TextSize.l} weight={TextWeight.bold}>{apartment.address}</Text>
            { apartment.metroStations.slice(0, 2).map((metro) => {
              return (
                <div className={s.metroContainer}>
                  <div className={s.metroColor} style={{ backgroundColor: metro.color }}/>
                  <Text size={TextSize.m}>{metro.name}</Text>
                  <Text size={TextSize.m} color={TextColor.light}>&nbsp;· {metro.time}</Text>
                </div>
              )
            }) }
          </div>
        </div>
      </CardSection>
      <CardSection>
        <div className={s.apartmentValues}>
          <div className={s.apartmentValueContainer}>
            <Icon className={s.apartmentValueIcon} icon={DoorIcon}/>
            <Text size={TextSize.s}>{apartment.rooms_number}</Text>
          </div>
          <div className={s.apartmentValueContainer}>
            <Icon className={s.apartmentValueIcon} icon={SquareIcon}/>
            <Text size={TextSize.s}>{apartment.total_area} / {apartment.living_area} м<sup>2</sup></Text>
          </div>
          <div className={s.apartmentValueContainer}>
            <Icon className={s.apartmentValueIcon} icon={StairsIcon}/>
            <Text size={TextSize.s}>{apartment.floor_number} / {apartment.total_floors}</Text>
          </div>
        </div>
      </CardSection>
      <CardSection>
        <div className={s.price}>
          <Text size={TextSize.l} weight={TextWeight.bold}>
            {apartment.price.amount.toLocaleString()}₽
          </Text>
          {props.additionalInfo}
        </div>
      </CardSection>
      {after}
    </Card>
  )
};

export default ApartmentCard;
