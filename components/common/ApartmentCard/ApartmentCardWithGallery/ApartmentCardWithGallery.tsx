import React, {FC, Fragment, useCallback, useEffect, useRef, useState} from "react";
import ApartmentCard from "../ApartmentCard";
import {ApiApartment} from "../../../../types/Apartments";
import {DateTime} from "luxon";
import $ from "jquery";
import s from "./ApartmentCardWithGallery.module.scss";
import CircleIcon, {CircleIconSize, CircleIconTheme} from "../../ui/CircleIcon/CircleIcon";
import classNames from "classnames";
import {ReactComponent as LongArrow} from "../../../../assets/longArrow.svg";
import {ReactComponent as ClockIcon} from "../../../../assets/clock.svg";
import getRelativeDateLabel from "../../../../utils/getRalativeDateLabel";
import Tag, {TagSize} from "../../ui/Tag/Tag";
import Text, {TextSize, TextWeight} from "../../ui/Text/Text";
import _ from 'lodash';
import useWindowWidth from "../../../../hooks/useWindowWidth";
import ReactResizeDetector from "react-resize-detector";
import TinySlider, {TinySliderSettings} from "tiny-slider-react";

export type ApartmentCardWithGalleryApi = {
  init(): void,
};

type ImageSliderProps = {
  apartment: ApiApartment,
  imageIndex: number,
  onChangeImageIndex(index: number): void,
};

const sliderSetting: TinySliderSettings = {
  loop: true,
  lazyload: true,
  nav: false,
  controls: false,
};

const ImageSlider: FC<ImageSliderProps> = (props) => {
  const { apartment, imageIndex, onChangeImageIndex } = props;

  const [isHover, setIsHover] = useState(false);
  const freeTime = apartment.freeTime ? DateTime.fromMillis(apartment.freeTime) : null;
  const sliderRef = useRef<any>(null);
  const width = useWindowWidth();
  const isDesktop = width > 991;

  const prev = () => {
    onChangeImageIndex(imageIndex === 0 ? apartment.primary_pictures.length - 1 : imageIndex - 1);
    if (sliderRef.current) {
      sliderRef.current.slider.goTo('prev');
    }
  };

  const next = () => {
    onChangeImageIndex(imageIndex === apartment.primary_pictures.length - 1 ? 0 : imageIndex + 1);
    if (sliderRef.current) {
      sliderRef.current.slider.goTo('next');
    }
  };

  const toSlide = (index: number) => {
    onChangeImageIndex(index);
    if (sliderRef.current) {
      sliderRef.current.slider.goTo(index);
    }
  };

  const isSoonFree = freeTime ? freeTime.diff(DateTime.local(), 'days').days <= 1 : false;

  return (
    <div
      className={s.cardSliderContainer}
      onMouseEnter={() => setIsHover(true)}
      onPointerEnter={() => setIsHover(true)}
      onMouseLeave={() => setIsHover(false)}
      onPointerLeave={() => setIsHover(false)}
    >
      { isDesktop && (
        <Fragment>
          <CircleIcon
            className={classNames(s.navButton, s.prev, isHover ? s.active : null)}
            theme={CircleIconTheme.white}
            size={CircleIconSize.s}
            onClick={prev}
            icon={LongArrow}
          />
          <CircleIcon
            className={classNames(s.navButton, s.next, isHover ? s.active : null)}
            theme={CircleIconTheme.white}
            size={CircleIconSize.s}
            onClick={next}
            icon={LongArrow}
          />
        </Fragment>
      )}
      <div>
        {freeTime && (
          <Tag
            className={classNames(s.apartmentTimeToFree, { [s.apartmentSoonFree]: isSoonFree })}
            icon={ClockIcon}
            size={TagSize.s}
          ><Text weight={TextWeight.bold} size={TextSize.xs}>{getRelativeDateLabel(freeTime)}</Text></Tag>
        )}
        <div className={s.dots}>
          {_.range(apartment.primary_pictures.length).map((index) => {
            return (
              <div
                className={classNames(s.dot, imageIndex === index ? s.active : null)}
                key={index}
                onClick={() => toSlide(index)}
              />
            )
          })}
        </div>
        <div className={classNames(s.apartmentImageContainer)}>
          <TinySlider settings={sliderSetting} ref={sliderRef}>
            {apartment.primary_pictures.map((image, index) => {
              const showMore = apartment.pictures.length > apartment.primary_pictures.length
                && index === apartment.primary_pictures.length - 1;
              return (
                <div key={index} className={s.slide}>
                  <img
                    className={classNames(s.apartmentImage, 'tns-lazy-img', showMore ? s.withBlur : null)}
                    data-src={image.uri}
                    alt={image.filename}
                  />
                  { showMore && (
                    <div className={s.showMore}>
                      <Text size={TextSize.h1} weight={TextWeight.bold}>
                        +{apartment.pictures.length - apartment.primary_pictures.length}
                      </Text>
                    </div>
                  ) }
                </div>
              )
            })}
          </TinySlider>
        </div>
      </div>
    </div>
  )
};

type Props = {
  apartment: ApiApartment,
  className?: string,
  container?: any,
  carouselOptions?: OwlCarousel.Options,
  apiRef?(api: ApartmentCardWithGalleryApi): void,
};

const ApartmentCardWithGallery: FC<Props> = ({ apartment, carouselOptions, className, apiRef }) => {
  const [cardWidth, setCardWidth] = useState(0);
  const cardElRef = useRef<HTMLDivElement | null>(null);
  const [imageIndex, setImageIndex] = useState(0);

  const setRef = (ref: HTMLDivElement | null) => {
    cardElRef.current = ref;
    if (!ref) return;
    if (apiRef) {
      apiRef({
        init: () => {
        }
      });
    }
  };

  useEffect(() => {
    if (!cardElRef.current) return;
    $('.cardSlider', cardElRef.current).trigger('refresh.owl.carousel')
  }, [cardWidth]);

  const renderImageSlider = () => {
    return (
      <ImageSlider
        apartment={apartment}
        imageIndex={imageIndex}
        onChangeImageIndex={setImageIndex}
      />
    );
  };

  return (
    <div ref={setRef} className={className}>
      <ReactResizeDetector handleWidth={true} onResize={(width) => setCardWidth(width)}>
        <ApartmentCard
          className={classNames(s.apartmentCardWithGallery, 'apartment-card-with-gallery')}
          before={renderImageSlider()}
          withBorder={true}
          apartment={apartment}
        />
      </ReactResizeDetector>
    </div>
  );
};

export default ApartmentCardWithGallery;
