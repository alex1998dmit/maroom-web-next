import React, {FC, ReactNode, useEffect, useState } from 'react';
import Header from './Header';
import {detect} from 'detect-browser';
import classNames from "classnames";
import s from './Page.scss';
import Text, {TextSize}  from '../ui/Text/Text';
import Button, {ButtonSize, ButtonTheme} from '../ui/Button/Button';
import CloseIcon from '../../../assets/close.svg';
import CircleIcon, {CircleIconSize, CircleIconTheme} from "../ui/CircleIcon/CircleIcon";

type Props = {
  alwaysWhiteHeader: boolean,
  hideHeaderOnScroll: boolean,
  className?: string,
  headerFooter?: ReactNode,
  onChangeHeaderVisible?(isVisible: boolean): void,
};

const Page: FC<Props> = ({
  children,
  className, // ???
  headerFooter, // ??
  onChangeHeaderVisible, // ??
  alwaysWhiteHeader,
  hideHeaderOnScroll,
}) => {
  const [isVisibleBrowserWarning, setIsVisibleBrowserWarning] = useState(() => {
    // TODO replace to another module
    const browser = detect();
    return !!browser && ['edge', 'ie', 'edge-ios'].includes(browser.name);
  });
  // TODO replace if we don't need this cause using next.js
  useEffect(() => {
    window.scrollTo(0,0);
  }, []);

  return (
    <div className={classNames(s.page, className)}>
      <Header
        footer={headerFooter}
        alwaysWhite={alwaysWhiteHeader}
        hideOnScroll={hideHeaderOnScroll}
        onChangeVisible={onChangeHeaderVisible}
      />
      <div>
        {children}
      </div>
      {/* <Footer /> */}
      {/* TODO replace this part to another module */}
      {/* TODO remake internet explorer warning */}
      {isVisibleBrowserWarning && (
        (<h2>Old version</h2>)
      )}
    </div>
  );
};

export default Page;
