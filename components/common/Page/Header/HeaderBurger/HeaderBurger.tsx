import React, {FC, useState} from "react";
import Icon from "../../../Icon/Icon";
import BurgerIcon from '../assets/burger.svg';
import CloseIcon from '../../../../../assets/close.svg';
import s from './HeaderBurger.module.scss';

import menuItems from "../data/menuItems";
import Heading, { HeadingSize } from '../../../ui/Heading/Heading';
import Button, {ButtonTheme} from "../../../ui/Button/Button";
import UserIcon from '../../../../../assets/user.svg';
// import useScroll from "../../../../../hooks/useScroll/useScroll";
// import useJivoSite from "../../../../../hooks/useJivoSite/useJivoSite";

const HeaderBurger: FC = () => {
  const [isOpen, setIsOpen] = useState(false);

  // const scrollApi = useScroll();
  // const jivoApi = useJivoSite();

  const toggleMenu = () => {
    // scrollApi.setAllowScroll('headerBurger', isOpen);
    if (isOpen) {
      // jivoApi.show('headerBurger');
    } else {
      // jivoApi.hide('headerBurger');
    }
    setIsOpen(!isOpen);
  };

  return (
    <div className={s.headerBurger}>
      <Icon className={s.burgerIcon} icon={isOpen ? CloseIcon : BurgerIcon} onClick={toggleMenu}/>
      {isOpen ? (
        <div className={s.headerContent}>
          <div className={s.headerMenu}>
            {menuItems.map((item) => {
               return (
                 <a key={item.label} className={s.menuItem} href={item.href}>
                   <Heading size={HeadingSize.h2}>{item.label}</Heading>
                 </a>
               )
             })}
           </div>
           <div className={s.actions}>
             <Button theme={ButtonTheme.primary} icon={UserIcon}>Войти</Button>
           </div>
        </div>
      ) : null}
    </div>
  );
};

export default HeaderBurger;
