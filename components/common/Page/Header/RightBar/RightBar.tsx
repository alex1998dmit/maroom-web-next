import React, {FC, Fragment} from "react";
import s from "../Header.module.scss";
import HeaderMenu from "./HeaderMenu";
import Button, {ButtonTheme} from "../../../ui/Button/Button";
import UserIcon from '../../../../../assets/user.svg';
import HeaderBurger from "../HeaderBurger/HeaderBurger";
// import useWindowWidth from "../../../../../hooks/useWindowWidth";

const RightBar: FC = () => {
  // const width = useWindowWidth();
  const isBrowser = true;

  return (
    <Fragment>
      { isBrowser ? (
        <div className={s.rightPanel}>
          <HeaderMenu/>
          <div className={s.buttonContainer}>
            <Button
              iconClassName={s.buttonIcon}
              activeIconClassName={s.activeButtonIcon}
              className={s.loginButton}
              icon={UserIcon}
              theme={ButtonTheme.outline}
            >Войти</Button>
          </div>
        </div>
      ) : (
        <div className={s.rightPanel}>
          <HeaderBurger />
        </div>
      ) }
    </Fragment>
  )
};

export default RightBar;