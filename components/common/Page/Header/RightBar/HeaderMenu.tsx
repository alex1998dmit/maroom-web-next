import React, {FC} from "react";
import s from '../Header.module.scss';
import Text, {TextSize} from "../../../ui/Text/Text";
import menuItems from "../data/menuItems";

const HeaderMenu: FC = () => {
  return (
    <div className={s.headerMenu}>
      {menuItems.map((item) => {
        return (
          <a key={item.label} className={s.menuItem} href={item.href}>
            <Text size={TextSize.m}>{item.label}</Text>
          </a>
        )
      })}
    </div>
  );
};

export default HeaderMenu;
