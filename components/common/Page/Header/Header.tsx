import React, {FC, ReactNode, useEffect, useState} from "react";
import s from './Header.module.scss';
import LogoIcon from '../../../../assets/logo.svg';
import MobileLogoIcon from '../../../../assets/logo.svg';
import classNames from "classnames";
import RightBar from "./RightBar/RightBar";
import Icon from "../../Icon/Icon";
// import useScroll from "../../../../hooks/useScroll/useScroll";
// import useWindowWidth from "../../../../hooks/useWindowWidth";

type Props = {
  alwaysWhite: boolean,
  hideOnScroll: boolean,
  footer?: ReactNode,
  onChangeVisible?(isVisible: boolean): void,
};

const Header: FC<Props> = ({ alwaysWhite, hideOnScroll, footer, onChangeVisible }) => {
  const [isHeaderVisible, setIsHeaderVisible] = useState(true);
  // const [scrollDownOffset, setScrollDownOffset] = useState(0);

  // const scroll = useScroll();
  // const history = useHistory();
  // const width = useWindowWidth();


  const isDesktop = true;

  // useEffect(() => {
  //   onChangeVisible && onChangeVisible(isHeaderVisible)
  // }, [isHeaderVisible]);

  // useEffect(() => {
  //   if (!scroll.pos || !scroll.prevPos) return;
  //   if (scroll.pos.y === scroll.prevPos.y) return;
  //   const offset = scroll.pos.y - scroll.prevPos.y;
  //   setScrollDownOffset(scrollDownOffset * offset > 0 ? scrollDownOffset + offset : offset);
  // }, [scroll.pos, scroll.prevPos]);

  // useEffect(() => {
  //   if (scrollDownOffset < -10 && !isHeaderVisible) {
  //     setIsHeaderVisible(true);
  //   }
  //   if (scrollDownOffset > 10 && isHeaderVisible) {
  //     setIsHeaderVisible(false);
  //   }
  // }, [scrollDownOffset, isHeaderVisible]);

  // if (!scroll.pos) return null;

      // [s.onTop]: scroll.pos.y <= 50 && !alwaysWhite,

  const className = classNames(s.headerContent, {
    [s.mobile]: !isDesktop,
    [s.desktop]: isDesktop,
  });

  return (
    <div className={classNames(s.header, isDesktop && s.desktop, hideOnScroll && !isHeaderVisible && s.hide)}>
      <div className={className}>
        <Icon className={s.logo} icon={isDesktop ? LogoIcon : MobileLogoIcon}/>
        <RightBar/>
      </div>
      <div className={s.footer}>
        {footer}
      </div>
    </div>
  )
};

export default Header;