import s from './Footer.module.scss';
import React, {FC, Fragment} from "react";
import Container from '../../../ui/';
import {ReactComponent as LogoIcon} from "../../../../assets/logo.svg";
import {ReactComponent as FacebookIcon} from "../../../../assets/facebook.svg";
import {ReactComponent as TwitterIcon} from "../../../../assets/twitter.svg";
import {ReactComponent as YoutubeIcon} from "../../../../assets/youtube.svg";
import {ReactComponent as TelegramIcon} from "../../../../assets/telegram.svg";
import {ReactComponent as VkIcon} from "../../../../assets/vk.svg";
import {ReactComponent as InstagramIcon} from "../../../../assets/instagram.svg";
import Icon from "../../../ui/Icon";
// TODO replace
import useWindowWidth from "../../../../hooks/useWindowWidth";
import Text, {TextSize, TextWeight} from "../../../ui/Text";
import classNames from "classnames";
import _ from 'lodash';

type LinkGroups = Array<{
  label: string,
  links: Array<{ label: string, href: string }>
}>;

const linkGroups: LinkGroups = [{
  label: 'Maroom',
  links: [{
    label: 'О нас', href: '#',
  }, {
    label: 'FAQ', href: '#',
  }, {
    label: 'Блог', href: '#',
  }, {
    label: 'Пресса', href: '#',
  }, {
    label: 'Контакты', href: '/contacts',
  }]
}, {
  label: 'Владельцу',
  links: [{
    label: 'Как это работает', href: '#',
  }, {
    label: 'Сдать квартиру', href: '#',
  }, {
    label: 'Вопросы и ответы', href: '#',
  }, {
    label: 'Помощь', href: '#',
  }]
}, {
  label: 'Жильцу',
  links: [{
    label: 'Как это работает', href: '#',
  }, {
    label: 'Сдать квартиру', href: '#',
  }, {
    label: 'Вопросы и ответы', href: '#',
  }, {
    label: 'Помощь', href: '#',
  }]
}, {
  label: 'Партнерство',
  links: [{
    label: 'Для застройщиков', href: '#'
  }, {
    label: 'Для агентов', href: '#'
  }, {
    label: 'Для инвесторов', href: '#'
  }]
}, {
  label: 'Помощь',
  links: [{
    label: 'Поддержка', href: '#'
  }, {
    label: 'Конфиденциальность', href: '#'
  }, {
    label: 'Карта сайта', href: '#'
  }]
}];

const Footer: FC = () => {
  const width = useWindowWidth();
  const isDesktop = width > 991;
  const deviceClass = isDesktop ? s.desktop : s.mobile;

  return (
    <div className={classNames(s.footer, deviceClass)}>
      <div className={s.infoContainer}>
        <Container className={s.info}>
          <div className={s.left}>
            <Icon className={s.logo} icon={LogoIcon} />
            <Text size={isDesktop ? TextSize.m : TextSize.xs}>
              ООО “Марум” ©2018<br/>
              Все права защищены
            </Text>
          </div>
          <div className={s.right}>
            <a target='_blank' className={s.socialLink} href='/#'>
              <Icon className={classNames(s.socialIcon, s.facebookIcon)} icon={FacebookIcon}/>
            </a>
            <a target='_blank' className={s.socialLink} href='/#'>
              <Icon className={classNames(s.socialIcon, s.twitterIcon)} icon={TwitterIcon}/>
            </a>
            <a target='_blank' className={s.socialLink} href='/#'>
              <Icon className={classNames(s.socialIcon, s.youtubeIcon)} icon={YoutubeIcon}/>
            </a>
            <a target='_blank' className={s.socialLink} href='/#'>
              <Icon className={classNames(s.socialIcon, s.telegramIcon)} icon={TelegramIcon}/>
            </a>
            <a target='_blank' className={s.socialLink} href='/#'>
              <Icon className={classNames(s.socialIcon, s.vkIcon)} icon={VkIcon}/>
            </a>
            <a target='_blank' className={s.socialLink} href='/#'>
              <Icon className={classNames(s.socialIcon, s.instagramIcon)} icon={InstagramIcon}/>
            </a>
          </div>
        </Container>
      </div>
      <Container className={classNames(s.links, deviceClass)}>
        { isDesktop ? (
          linkGroups.map((group) => {
            return (
              <div className={s.linkGroup} key={group.label}>
                <Text size={TextSize.l} weight={TextWeight.bold}>{group.label}</Text>
                {group.links.map((link) => {
                  return (
                    <Text key={link.label} size={TextSize.m}>
                      <a href={link.href} className={s.link}>{link.label}</a>
                    </Text>
                  );
                })}
              </div>
            )
          })
        ) : (
          <Fragment>
            <div className={classNames(s.linksRow, s.rightRow)}>
              {_.compact(linkGroups.map((group, index) => {
                if (index % 2 === 1) return null;
                return (
                  <div className={s.linkGroup} key={group.label}>
                    <Text size={TextSize.l} weight={TextWeight.bold}>{group.label}</Text>
                    {group.links.map((link) => {
                      return (
                        <Text key={link.label} size={TextSize.m}>
                          <a href={link.href} className={s.link}>{link.label}</a>
                        </Text>
                      );
                    })}
                  </div>
                );
              }))}
            </div>
            <div className={s.linksRow}>
              {_.compact(linkGroups.map((group, index) => {
                if (index % 2 === 0) return null;
                return (
                  <div className={classNames(s.linkGroup, deviceClass)} key={group.label}>
                    <Text size={TextSize.l} weight={TextWeight.bold}>{group.label}</Text>
                    {group.links.map((link) => {
                      return (
                        <Text key={link.label} size={TextSize.m}>
                          <a href={link.href} className={s.link}>{link.label}</a>
                        </Text>
                      );
                    })}
                  </div>
                );
              }))}
            </div>
          </Fragment>
        )}
      </Container>
    </div>
  );
};

export default Footer;
