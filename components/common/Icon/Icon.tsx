import React, {FC, SVGProps} from "react";
import s from './Icon.module.scss';
import classNames from "classnames";

export type IconProps = {
  icon: FC<SVGProps<SVGSVGElement>>,
  hoverIcon?: FC<SVGProps<SVGSVGElement>>,
  disabled?: boolean,
  primary?: boolean,
  className?: string,
  onClick?(): void,
};

const Icon: FC<IconProps> = (props) => {
  const CustomIcon = props.icon;
  const className = classNames(props.className, s.icon, {
    [s.disabled]: props.disabled,
    [s.primary]: props.primary,
  });
  return (
    <CustomIcon onClick={props.onClick} className={className} />
  );
};

export default Icon;
