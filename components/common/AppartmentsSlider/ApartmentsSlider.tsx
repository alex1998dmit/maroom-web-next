import React, {FC, useCallback, useEffect, useRef, useState} from "react";
import s from './ApartmentsSlider.module.scss';
import Text, {TextSize, TextWeight} from "../ui/Text/Text";
import CircleIcon, {CircleIconSize, CircleIconTheme} from "../ui/CircleIcon/CircleIcon";
import LongArrow from "../../../assets/longArrow.svg";
import ShortArrow from "../../../assets/shortArrow.svg";
import classNames from "classnames";
import TinySlider, {TinySliderSettings} from "tiny-slider-react";
import $ from 'jquery';
import Button, {ButtonTheme} from "../ui/Button/Button";
import 'owl.carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import useWindowWidth from "../../../hooks/useWindowWidth";
import ApartmentCardWithGallery, {ApartmentCardWithGalleryApi} from "../ApartmentCard/ApartmentCardWithGallery/ApartmentCardWithGallery";
import { apartamentsStoreType } from '../../../store/Apartments';

(window as any).$ = $;

type Props = {
  apartments: apartamentsStoreType,
  title: string,
  className?: string
} & ({
  showAllApartmentsButton: false,
} | {
  showAllApartmentsButton: true,
  total: number,
  allApartmentsLink: string,
});

const cardCarouselOptions = {
  loop: true,
  margin: 0,
  items: 1,
  responsive: {
    0: {
      mouseDrag: true,
      touchDrag: true,
    },
    991: {
      mouseDrag: false,
      touchDrag: false,
    }
  }
};

const sliderSettings: TinySliderSettings = {
  nav: false,
  controls: false,
  loop: false,
  responsive: {
    0: {
      items: 1,
      gutter: 12,
    },
    991: {
      items: 3,
    },
    1408: {
      items: 4,
      gutter: 16
    }
  }
};

const ApartmentsSlider: FC<Props> = ({ apartments, className, title }) => {
  const sliderRef = useRef<any>(null);
  const cardApis = useRef<ApartmentCardWithGalleryApi[]>([]);

  const [isFirst, setIsFirst] = useState(true);
  const [isLast, setIsLast] = useState(true);

  const width = useWindowWidth();

  const isDesktop = width > 991;
  const deviceClass = isDesktop ? s.desktop : s.mobile;

  useEffect(() => {
    setIsLast(apartments.length === 0);
  }, [apartments]);

  const checkSliderPosition = () => {
    const { index, items } = sliderRef.current && sliderRef.current.slider.getInfo();
    setIsFirst(index === 0);
    setIsLast(index + items === apartments.length);
  };

  const prev = useCallback(() => {
    if (sliderRef.current) {
      sliderRef.current.slider.goTo('prev');
      checkSliderPosition();
    }
  }, []);

  const next = useCallback(() => {
    if (sliderRef.current) {
      sliderRef.current.slider.goTo('next');
      checkSliderPosition();
    }
  }, [apartments.length]);

  return (
    <div className={classNames(s.apartmentsSlider, className, deviceClass)}>
      <div className={s.header}>
        <Text size={TextSize.h2} weight={TextWeight.bold}>{title}</Text>
        <div className={s.navigation}>
          <CircleIcon
            className={classNames(s.navButton, s.prev, isFirst ? s.disabled : null)}
            theme={CircleIconTheme.white}
            size={CircleIconSize.m}
            onClick={prev}
            icon={isFirst ? ShortArrow : LongArrow}
            withBorder={true}
            disabled={isFirst}
          />
          <CircleIcon
            className={classNames(s.navButton, s.next, isLast ? s.disabled : null)}
            theme={CircleIconTheme.white}
            size={CircleIconSize.m}
            onClick={next}
            icon={isLast ? ShortArrow : LongArrow}
            withBorder={true}
            disabled={isLast}
          />
        </div>
      </div>
      <div  className={classNames(s.cards, width > 1024 && s.desktop)}>
        <TinySlider settings={sliderSettings} ref={sliderRef}>
          {apartments.map((apartment, index) => {
            return (
              <ApartmentCardWithGallery
                className={classNames(s.card, width > 1024 && s.desktop)}
                key={index}
                apartment={apartment}
                apiRef={(api) => cardApis.current.push(api)}
                carouselOptions={cardCarouselOptions}
              />
            )
          })}
        </TinySlider>
      </div>
      <Button
        className={s.allApartmentsLink}
        theme={ButtonTheme.primary}
        count={11}
      >Все квартиры</Button>
    </div>
  )
};

export default ApartmentsSlider;
