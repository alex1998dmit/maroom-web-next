import React, {FC, DetailedHTMLProps, HTMLAttributes} from "react";
import classNames from "classnames";
import s from './Heading.module.scss';

export enum HeadingSize {
  h1 = 'h1',
  h2 = 'h2',
  h3 = 'h3'
}

type Props = DetailedHTMLProps<HTMLAttributes<HTMLHeadingElement>, HTMLHeadingElement> & {
  size: HeadingSize
};

const Heading: FC<Props> = (props) => {
  return (
    <div {...props} className={classNames(props.className, s.heading, s[props.size])}/>
  )
};

export default Heading;
