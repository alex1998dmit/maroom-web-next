import React, {FC} from "react";
import classNames from "classnames";
import s from './Container.module.scss';

type Props = {
  className?: string,
  isWide?: boolean,
};

const Container: FC<Props> = (props) => {
  return (
    <div className={classNames(props.className, s.container, props.isWide && s.wide)}>
      {props.children}
    </div>
  )
};

export default Container;
