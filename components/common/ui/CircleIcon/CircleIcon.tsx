import React, {FC, useCallback} from "react";
import classNames from "classnames";
import s from './CircleIcon.module.scss';
import Icon, {IconProps} from "../../Icon/Icon";

export enum CircleIconTheme {
  white = 'white',
  gray = 'gray',
}

export enum CircleIconSize {
  s = 's',
  m = 'm',
  l = 'l',
}

const themeClasses = {
  [CircleIconTheme.gray]: s.themeGray,
  [CircleIconTheme.white]: s.themeWhite,
};

const sizeClasses = {
  [CircleIconSize.s]: s.sizeS,
  [CircleIconSize.m]: s.sizeM,
  [CircleIconSize.l]: s.sizeL,
};

type Props = {
  icon: IconProps['icon'],
  theme: CircleIconTheme,
  size: CircleIconSize,
  disabled?: boolean,
  className?: string,
  withBorder?: boolean,
  onClick?(): void,
}

const CircleIcon: FC<Props> = (props) => {
  const { disabled, onClick } = props;
  const handleClick = useCallback(() => {
    if (disabled || !onClick) return;
    onClick();
  }, [disabled, onClick]);

  const className = classNames(props.className, s.icon, sizeClasses[props.size], themeClasses[props.theme], {
    [s.withBorder]: props.withBorder,
  });

  return (
    <div className={className} onClick={handleClick}>
      <Icon disabled={props.disabled} icon={props.icon}/>
    </div>
  )
};

export default CircleIcon;
