import React, {FC, DetailedHTMLProps, ButtonHTMLAttributes, SVGProps} from "react";
import classNames from "classnames";
import s from './CircleButton.module.scss';

export enum CircleButtonTheme {
  primary = 'primary',
  outline = 'outline'
}

export enum CircleButtonSize {
  s = 's',
  m = 'm',
}

const themeClasses = {
  [CircleButtonTheme.primary]: s.circleButtonThemePrimary,
  [CircleButtonTheme.outline]: s.circleButtonThemeOutline,
};

type Props = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
  theme: CircleButtonTheme,
  size?: CircleButtonSize,
  icon: FC<SVGProps<SVGSVGElement>>,
  innerRef?: (ref: HTMLButtonElement) => void
};

const CircleButton: FC<Props> = (props) => {
  const {
    icon: Icon,
    children,
    innerRef,
    ...buttonProps
  } = props;

  return (
    <button
      {...buttonProps}
      ref={innerRef}
      className={classNames(themeClasses[props.theme], s.circleButton, props.className)}
    >
      <Icon className={s.icon}/>
    </button>
  )
};

export default CircleButton;
