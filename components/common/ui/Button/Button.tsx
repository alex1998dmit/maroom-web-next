import React, {FC, DetailedHTMLProps, ButtonHTMLAttributes, SVGProps, useRef, useState, useCallback} from "react";
import classNames from "classnames";
import s from './Button.module.scss';
import Text, {TextSize} from "../Text/Text";
import $ from 'jquery';

export enum ButtonTheme {
  primary = 'primary',
  outline = 'outline',
  white = 'white'
}

export enum ButtonSize {
  xs = 'xs',
  s = 's',
  m = 'm',
  l = 'l'
}

export enum ButtonIconPosition {
  right = 'right',
  left = 'left',
}

const sizeClasses = {
  [ButtonSize.xs]: s.buttonSizeXS,
  [ButtonSize.s]: s.buttonSizeS,
  [ButtonSize.m]: s.buttonSizeM,
  [ButtonSize.l]: s.buttonSizeL,
};

const textSize = {
  [ButtonSize.xs]: TextSize.s,
  [ButtonSize.s]: TextSize.s,
  [ButtonSize.m]: TextSize.m,
  [ButtonSize.l]: TextSize.m,
};

const themeClasses = {
  [ButtonTheme.primary]: s.buttonThemePrimary,
  [ButtonTheme.outline]: s.buttonThemeOutline,
  [ButtonTheme.white]: s.buttonThemeWhite,
};

type Props = DetailedHTMLProps<ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement> & {
  theme: ButtonTheme,
  onClick?(): void,
  size?: ButtonSize,
  width?: number,
  iconPosition?: ButtonIconPosition,
  icon?: FC<SVGProps<SVGSVGElement>>,
  count?: number,
  iconClassName?: string,
  activeIconClassName?: string,
};

const Button: FC<Props> = (props) => {
  const ref = useRef<HTMLButtonElement | null>(null);
  const [showEffect, setShowEffect] = useState(false);
  const [effectPos, setEffectPos] = useState<{top: string, left: string} | null>(null);
  const [isHover, setIsHover] = useState(false);
  const [isClicking, setIsClicking] = useState(false);

  const {
    width,
    size = ButtonSize.m,
    iconPosition = ButtonIconPosition.left,
    icon: Icon,
    iconClassName,
    children,
    count,
    activeIconClassName,
    ...buttonProps
  } = props;

  const clickEffect = (e: any) => {
    if (!ref.current) return;
    const $ref = $(ref.current);
    const offset = $ref.offset();
    if (!offset) return;
    setShowEffect(true);
    setEffectPos({
      top: `${e.pageY - offset.top}px`,
      left: `${e.pageX - offset.left}px`
    });
    setIsClicking(true);
  };

  const className = classNames(
    themeClasses[props.theme],
    sizeClasses[size],
    s.button,
    props.disabled && s.disabled,
    props.className,
  );

  const onMouseEnter = useCallback(() => setIsHover(true), []);
  const onMouseLeave = useCallback(() => {
    setIsHover(false);
    setIsClicking(false);
  }, []);

  const onMouseUp = useCallback(() => {
    setShowEffect(false);
    if (isClicking && !props.disabled && props.onClick) {
      props.onClick();
    }
    setIsClicking(false);
  }, [isClicking]);

  return (
    <button
      {...buttonProps}
      ref={ref}
      onMouseDown={clickEffect}
      onMouseUp={onMouseUp}
      onMouseEnter={onMouseEnter}
      onPointerEnter={onMouseEnter}
      onMouseLeave={onMouseLeave}
      onPointerLeave={onMouseLeave}
      className={className}
      style={{ width }}
    >
      { showEffect ? (
        <div className={s.clickEffectContainer}>
          <div className={s.clickEffect} style={effectPos || {}}/>
        </div>
      ) : null}
      { Icon && iconPosition === ButtonIconPosition.left ? (
        <Icon className={classNames(
          s.leftIcon,
          iconClassName,
          isHover && activeIconClassName,

        )}/>
      ) : null }
      <Text size={textSize[size]} className={s.label}>{children}</Text>
      { Icon && iconPosition === ButtonIconPosition.right ? (
        <Icon className={classNames(s.rightIcon, iconClassName, isHover && activeIconClassName)}/>
      ) : null }
      { count != null ? <div className={s.count}>{count}</div> : null }
    </button>
  )
};

export default Button;
