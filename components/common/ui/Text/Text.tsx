import React, {FC, DetailedHTMLProps, HTMLAttributes} from 'react';
import s from './Text.module.scss';
import classNames from 'classnames';

export enum TextSize {
  xs = 'xs',
  s = 's',
  m = 'm',
  l = 'l',
  h1 = 'h1',
  h2 = 'h2',
  h3 = 'h3',
}

export enum TextWeight {
  medium = 'medium',
  bold = 'bold'
}

export enum TextColor {
  black = 'black',
  light = 'light',
  white = 'white',
}

const sizeClasses = {
  [TextSize.xs]: s.textSizeXS,
  [TextSize.s]: s.textSizeS,
  [TextSize.m]: s.textSizeM,
  [TextSize.l]: s.textSizeL,
  [TextSize.h1]: s.textSizeH1,
  [TextSize.h2]: s.textSizeH2,
  [TextSize.h3]: s.textSizeH3,
};

const weightClasses = {
  [TextWeight.medium]: s.textWeightMedium,
  [TextWeight.bold]: s.textWeightBold
};

const colorsClasses = {
  [TextColor.black]: s.colorBlack,
  [TextColor.light]: s.colorLight,
  [TextColor.white]: s.colorWhite,
};

type Props = DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> & {
  size: TextSize,
  weight?: TextWeight
  color?: TextColor,
};

const Text: FC<Props> = (props) => {
  const {
    weight = TextWeight.medium,
    color,
    ...divProps
  } = props;

  const className = classNames(
    props.className,
    s.text,
    sizeClasses[props.size],
    weightClasses[weight],
    color && colorsClasses[color]
  );

  return (
    <div {...divProps} className={className}/>
  );
};

export default Text;
