import {Picture} from "./common";

export type MetroStation = {
  color: string,
  name: string,
  time: string,
}

export enum ApartmentCurrency {
  rub = 'rub',
}

type Feature = [{
  resource_id: number,
  tag: string,
  section: string,
  amount: number,
  description: string,
  pictures: Picture[]
}];

export type ApiApartment = {
  resource_id: number,
  primary_pictures: Picture[],
  pictures: Picture[],
  features: Feature[],
  description: string,
  address: string,
  longitude: number,
  latitude: number,
  house_number: number,
  apartment_number: number,
  rooms_number: number,
  floor_number: number,
  total_floors: number,
  total_area: number,
  living_area: number,
  price: {
    amount: number,
    deposit: number,
    currency: ApartmentCurrency
  }

  // mocks
  metroStations: MetroStation[],
  tour3DLink?: string,
  freeTime?: number,
  isLeased: boolean,
};

export type ApartmentMock = {
  id: string,
  mainImages: string[],
  images: string[],
  address: string,
  metroStations: MetroStation[],
  roomsCount: number,
  totalSpace: number,
  livingSpace: number,
  floor: number,
  totalFloors: number,
  price: number,
  description?: string,
  isLeased: boolean,
  freeTime: number | null,
  lt: number,
  ln: number
  tour3DLink?: string,
};
