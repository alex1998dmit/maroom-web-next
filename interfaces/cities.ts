export type City = {
  name: string,
  image: string,
} & ({
  isAvailable: false
} | {
  isAvailable: true,
  apartments_count: number
});
