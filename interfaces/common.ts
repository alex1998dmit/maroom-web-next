export type Picture = {
  filename: string,
  extension: string,
  size: number,
  content_type: string,
  uri: string
}