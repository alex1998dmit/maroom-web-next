const withSass = require('@zeit/next-sass')
const withImages = require('next-images');
const withCSS = require("@zeit/next-css");

module.exports = withSass({
  cssModules: true,
  webpack(config) {
    config.module.rules.push({
      test: /\.svg$/,
      issuer: {
        test: /\.(js|ts)x?$/,
      },
      use: ['@svgr/webpack'],
    });
    return config;
  },
})

// module.exports = {
//   webpack(config) {
//     config.module.rules.push({
//       test: /\.svg$/,
//       issuer: {
//         test: /\.(js|ts)x?$/,
//       },
//       use: ['@svgr/webpack'],
//     });
//     return config;
//   },
// };

// module.exports = withCSS(
//   withSass(
//     withImages({
//       distDir: '../_next',
//       webpack(config, options) {
//         return config;
//       }
//     })
//   )
// );