import { observable, decorate, computed } from 'mobx';
import {City} from '../interfaces/cities';

class CitiesStore {
  data: City[] = [];
  state = 'default';
  error = {};

  constructor(defaultItems: City[]) {
    this.data = [...this.data, ...defaultItems];
  }

  getAllItems = () => {
    return this.items;
  }
};

decorate(CitiesStore, {
  data: observable,
  state: observable,
  error: observable,
  getAllItems: computed,
});

const defaultItems = [{
  image: '/images/cities/moscow.jpg',
  name: 'Москва',
  apartments_count: 68,
  isAvailable: true,
}, {
  image: '/images/cities/peter.jpg',
  name: 'Санкт-Петербург',
  apartments_count: 0,
  isAvailable: false,
}, {
  image: '/images/cities/sochi.jpg',
  name: 'Сочи',
  apartments_count: 0,
  isAvailable: false,
}];

const citiesStore = new CitiesStore(defaultItems);
export type citiesStoreType = typeof CitiesStore
export default citiesStore
