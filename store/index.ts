import ApartmentsStore from './Apartments';
import CitiesStore from './Cities';

let store = null;

const Stores = () => {
  return {
    apartments: ApartmentsStore,
    cities: CitiesStore,
  };
};

export type storesType = typeof Stores;

export function initializeStore(isServer: boolean) : object {
  if (isServer) {
    return Stores();
  } else {
    if (store === null) {
      store = Stores();
    }
    return store;
  }
};