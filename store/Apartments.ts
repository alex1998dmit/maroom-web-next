import { action, observable, decorate, runInAction, configure } from 'mobx';
import to from 'await-to-js';
import axios from 'axios';

configure({ enforceActions: true });

class ApartmentsStore {
  apartments = {};
  state = 'default';
  error = {};

  fetchApartments = async () => {
    const axiosOptions = {
      params: {
        max_depth: 2
      }
    };
    const [err, resp] = await to(
      axios({
        method: 'GET',
        url: `https://api.dev.maroom.ru/api/v1/apartments/`,
        timeout: Infinity,
        responseType: 'json',
        headers: {
          'content-type': 'application/json',
        },
        ...axiosOptions,
      }));
    if (err) {
      runInAction(() => {
        this.state = 'error';
        this.error = err;
      })
    } else {
      runInAction(() => {
        this.state = 'done';
        this.apartments = resp.data;
      })
    }
  };
}

decorate(ApartmentsStore, {
  apartments: observable,
  state: observable,
  error: observable,
  fetchApartments: action,
});

const apartmentsStore = new ApartmentsStore();
export type apartamentsStoreType = typeof ApartmentsStore;
export default apartmentsStore;
