import _ from 'lodash';
import axios, { AxiosRequestConfig } from 'axios';

export type ApiError = {
  code: string,
  message: string
} | {
  code: 'cancelRequest'
};

export type ApiResponse<TSuccess, TError = ApiError> =
  ({ status: 'success', data: TSuccess }) |
  ({ status: 'error', data: TError | ApiError });


type Options = Omit<AxiosRequestConfig, 'method' | 'url'> & {
  token?: string,
};

export const api = {
  async fetch<TResponse>(
    method: AxiosRequestConfig['method'],
    url: string,
    options: Options = {}
  ): Promise<TResponse> {
    const { token, ...axiosOptions } = options;
    const response = await axios({
      method,
      url: `https://api.dev.maroom.ru/api/v1${url}`,
      timeout: Infinity,
      responseType: 'json',
      headers: {
        ...axiosOptions.headers,
        'content-type': 'application/json',
      },
      ...axiosOptions,
      transformResponse(data) {
        if (_.isNull(data)) {
          return {
            status: 'error',
            data: {code: 'internalError', message: 'Непредвиденная ошибка сервера'}
          };
        }
        return data;
      }
    }).catch((err): { data: ApiResponse<ApiError> } => {
      if (axios.isCancel(err)) {
        return {
          data: {
            status: 'error',
            data: { code: 'cancelRequest' }
          }
        }
      }

      const {response} = err;
      if (!response) {
        return {
          data: {
            status: 'error',
            data: {code: 'networkError', message: 'Сервер временно недоступен'}
          }
        };
      }
      return response;
    });

    return response.data;
  }
};

export default api;
