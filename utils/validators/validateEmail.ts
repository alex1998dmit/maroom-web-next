export const validateEmail = (msg = 'Введите корректный email') => (value: string) => {
  if (value.match(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/)) {
    return null;
  }
  return msg;
};
