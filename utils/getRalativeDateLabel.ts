import {DateTime} from "luxon";

const getRelativeDateLabel = (dateTime: DateTime) => {
  const now = DateTime.local();
  if (dateTime.diff(now, 'days').days <= 1) {
    return `${dateTime.toRelativeCalendar()} в ${dateTime.toFormat('HH:mm')}`
  }
  return dateTime.setLocale('ru').toFormat('dd MMMM');
};

export default getRelativeDateLabel;
